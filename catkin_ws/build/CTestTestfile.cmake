# CMake generated Testfile for 
# Source directory: /home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/src
# Build directory: /home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("laikago_msgs")
subdirs("laikago_real")
