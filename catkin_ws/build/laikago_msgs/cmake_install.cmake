# Install script for directory: /home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/src/laikago_msgs

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/laikago_msgs/msg" TYPE FILE FILES
    "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/src/laikago_msgs/msg/MotorCmd.msg"
    "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/src/laikago_msgs/msg/MotorState.msg"
    "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/src/laikago_msgs/msg/Cartesian.msg"
    "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/src/laikago_msgs/msg/IMU.msg"
    "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/src/laikago_msgs/msg/LED.msg"
    "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/src/laikago_msgs/msg/LowCmd.msg"
    "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/src/laikago_msgs/msg/LowState.msg"
    "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/src/laikago_msgs/msg/HighCmd.msg"
    "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/src/laikago_msgs/msg/HighState.msg"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/laikago_msgs/cmake" TYPE FILE FILES "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/build/laikago_msgs/catkin_generated/installspace/laikago_msgs-msg-paths.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/devel/include/laikago_msgs")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/roseus/ros" TYPE DIRECTORY FILES "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/devel/share/roseus/ros/laikago_msgs")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common-lisp/ros" TYPE DIRECTORY FILES "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/devel/share/common-lisp/ros/laikago_msgs")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gennodejs/ros" TYPE DIRECTORY FILES "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/devel/share/gennodejs/ros/laikago_msgs")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process(COMMAND "/usr/bin/python2" -m compileall "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/devel/lib/python2.7/dist-packages/laikago_msgs")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages" TYPE DIRECTORY FILES "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/devel/lib/python2.7/dist-packages/laikago_msgs")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/build/laikago_msgs/catkin_generated/installspace/laikago_msgs.pc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/laikago_msgs/cmake" TYPE FILE FILES "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/build/laikago_msgs/catkin_generated/installspace/laikago_msgs-msg-extras.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/laikago_msgs/cmake" TYPE FILE FILES
    "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/build/laikago_msgs/catkin_generated/installspace/laikago_msgsConfig.cmake"
    "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/build/laikago_msgs/catkin_generated/installspace/laikago_msgsConfig-version.cmake"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/laikago_msgs" TYPE FILE FILES "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/src/laikago_msgs/package.xml")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/laikago_msgs" TYPE DIRECTORY FILES "/home/suntao/workspace/stbot/laikago_ws_ros/catkin_ws/src/laikago_msgs/include/laikago_msgs/" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

