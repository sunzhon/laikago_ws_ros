/************************************************************************
  Copyright (c) 2018-2019, Unitree Robotics.Co.Ltd. All rights reserved.
  Use of this source code is governed by the MPL-2.0 license, see LICENSE.
 ************************************************************************/

#include <ros/ros.h>
#include <pthread.h>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <laikago_msgs/LowCmd.h>
#include <laikago_msgs/LowState.h>
#include "laikago_sdk/laikago_sdk.hpp"
#include <std_msgs/Float32MultiArray.h>


using namespace laikago;
using namespace std;
static long motiontime = 0;
float Kv[3] = {0};  
float Kp[3] = {0};

unsigned long int Tpi = 0;
LowCmd SendLowLCM = {0};
LowState RecvLowLCM = {0};
laikago_msgs::LowCmd SendLowROS;
laikago_msgs::LowState RecvLowROS;

const unsigned int joint_num=12;
float motorSignal[12] = { 0, 0, 0,   // fr_joint0, fr_joint1, fr_joint2
    0, 0, 0,   // fl_joint0, fl_joint1, fl_joint2
    0, 0, 0,   // rr_joint0, rr_joint1, rr_joint2
    0, 0, 0 }; // rl_joint0, rl_joint1, rl_joint2



float motorCommand[joint_num] = { 0, 0, 0,   // fr_joint0, fr_joint1, fr_joint2
    0, 0, 0,   // fl_joint0, fl_joint1, fl_joint2
    0, 0, 0,   // rr_joint0, rr_joint1, rr_joint2
    0, 0, 0 }; // rl_joint0, rl_joint1, rl_joint2

constexpr int motorDir[12] = {  FR_0, FR_1, FR_2,
    FL_0, FL_1, FL_2,
    RR_0, RR_1, RR_2,
    RL_0, RL_1, RL_2 };




Control control(LOWLEVEL);
LCM roslcm;
boost::mutex mutex;


const float joint_controller_min[joint_num]={-1.0, -1.0, -1.0,
    -1.0, -1.0, -1.0,
    -1.0, -1.0, -1.0,
    -1.0, -1.0, -1.0};

const float joint_controller_max[joint_num]={1.0, 1.0, 1.0,
    1.0, 1.0, 1.0,
    1.0, 1.0, 1.0,
    1.0, 1.0, 1.0};

//const float  joint_motor_min[joint_num]={-1.051, -0.525, -2.7856,-1.051, -0.525, -2.7856,
//    -0.876,  -0.525, -2.7856, -0.876,  -0.525, -2.7856
//};
//const float joint_motor_max[joint_num]={0.876, 3.942, -0.613, 0.876, 3.942, -0.613,
//    1.051, 3.942, -0.613,  1.051, 3.942, -0.613
//};


const float joint_motor_min[joint_num]={1.051, 3.942, -0.613, 1.051, 3.942, -0.613,
    -1.051, 3.942, -0.613,  -1.051, 3.942, -0.613
};

const float  joint_motor_max[joint_num]={-0.876, -0.525, -2.7856, -0.876, -0.525, -2.7856,
    0.876,  -0.525, -2.7856, 0.876,  -0.525, -2.7856
};

FILE *f_st = fopen("filename.csv", "a");


std::vector<float> sensorySignals;
std_msgs::Float32MultiArray sensoryValues;


void* update_loop(void* data)
{
    while(ros::ok){
        boost::mutex::scoped_lock lock(mutex);
        roslcm.Recv();
        lock.unlock();
        usleep(2000);
    }
}
// motor callback function
void motorCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        motorSignal[i] = *it;
        i++;
        //std::cout << i << ": " << motorSignal[i]<< std::endl;
    }
    return;
}


int main(int argc, char *argv[])
{
    std::cout << "WARNING: Control level is set to LOW-level." << std::endl
        << "Make sure the robot is hung up." << std::endl
        << "Press Enter to continue..." << std::endl;
    std::cin.ignore();

    ros::init(argc, argv, "position_ros_mode");
    ros::NodeHandle n;
    ros::Rate loop_rate(500);
    roslcm.SubscribeState();

    pthread_t tid;
    pthread_create(&tid, NULL, update_loop, NULL);

    SendLowROS.levelFlag = LOWLEVEL;
    for(int i = 1; i<13; i++){
        SendLowROS.motorCmd[i].mode = 0x0A;   // motor switch to servo (PMSM) mode
    }

    ros::Subscriber motorSub = n.subscribe("/motorValues",1,motorCB);
     //pub
    ros::Publisher sensoryPub = n.advertise<std_msgs::Float32MultiArray>("/sensorValues",2);

    const unsigned int sensor_num=61;
    sensorySignals.resize(sensor_num);
    sensoryValues.data.resize(sensor_num);





    while (ros::ok()){
        motiontime++;
        roslcm.Get(RecvLowLCM);
        memcpy(&RecvLowROS, &RecvLowLCM, sizeof(LowState));
        //printf("%f\n",  RecvLowROS.footForce[0]);

        //sensorySignals.clear();
        for(unsigned int idx=0;idx<joint_num;idx++)
        sensorySignals.at(idx)=RecvLowROS.motorState[idx].position;

        for(unsigned int idx=0;idx<joint_num;idx++)
        sensorySignals.at(idx+12)=RecvLowROS.motorState[idx].velocity;

        for(unsigned int idx=0;idx<joint_num;idx++)
        sensorySignals.at(idx+24)=RecvLowROS.motorState[idx].torque;

        for(unsigned int idx=0;idx<joint_num;idx++)
        sensorySignals.at(idx+36)=RecvLowROS.motorState[idx].temperature;


        for(unsigned int idx=0;idx<3;idx++)
            sensorySignals.at(idx+48)=RecvLowROS.imu.rpy[idx];
        for(unsigned int idx=0;idx<3;idx++)
            sensorySignals.at(idx+51)=RecvLowROS.imu.acceleration[idx];
        for(unsigned int idx=0;idx<3;idx++)
            sensorySignals.at(idx+54)=RecvLowROS.imu.gyroscope[idx];

            sensorySignals.at(57)=RecvLowROS.footForce[0];
            sensorySignals.at(58)=RecvLowROS.footForce[2];
            sensorySignals.at(59)=RecvLowROS.footForce[1];
            sensorySignals.at(60)=RecvLowROS.footForce[3];


        for(uint8_t idx=0;idx<sensor_num;idx++)
            sensoryValues.data[idx]=sensorySignals.at(idx);

        sensoryPub.publish(sensoryValues);

        // gravity compensation
        SendLowROS.motorCmd[FR_0].torque = -0.65f;
        SendLowROS.motorCmd[FL_0].torque = +0.65f;
        SendLowROS.motorCmd[RR_0].torque = -0.65f;
        SendLowROS.motorCmd[RL_0].torque = +0.65f;

        if( motiontime >= 100) {
            if (motiontime == 100) {
                Kp[0] = 0.132;
                Kp[1] = 0.132;
                Kp[2] = 0.132;
                Kv[0] = 0.02;
                Kv[1] = 0.02;
                Kv[2] = 0.02;
            }

            if (motiontime == 500) {
                Kp[0] = 0.5;
                Kp[1] = 0.15;
                Kp[2] = 0.15;
                Kv[0] = 0.02;
                Kv[1] = 0.02;
                Kv[2] = 0.02;
            }

            if (motiontime == 900) {
                Kp[0] = 0.8;
                Kp[1] = 0.18;
                Kp[2] = 0.18;
                Kv[0] = 0.02;
                Kv[1] = 0.02;
                Kv[2] = 0.02;
            }

            if (motiontime == 1300) {
                Kp[0] = 1.2;
                Kp[1] = 0.22;
                Kp[2] = 0.22;
                Kv[0] = 0.02;
                Kv[1] = 0.02;
                Kv[2] = 0.02;
            }

            if (motiontime == 1700) {
                Kp[0] = 1.84*8;
                Kp[1] = 0.263*8;
                Kp[2] = 0.263*8;

                Kv[0] = 0.02*8;
                Kv[1] = 0.02*8;
                Kv[2] = 0.02*8;
            }

            // Move leg
            if (motiontime > 100) {
                for(unsigned int idx=0; idx<joint_num; idx++){
                    motorCommand[idx] = (joint_motor_max[idx]-joint_motor_min[idx])/(joint_controller_max[idx] - joint_controller_min[idx])*(motorSignal[idx]-joint_controller_min[idx])+joint_motor_min[idx];
                    // printf("%f\n",motorCommand[idx]);
                }
                if(f_st)
                {
                    ;   // fprintf(f_st, "%f\t%f\t%f\n", motorCommand[0], motorCommand[1], motorCommand[2]);
                }
                else printf("Unable to open the target file\n");


                //------------ Front right
                SendLowROS.motorCmd[FR_0].position = motorCommand[0];
                SendLowROS.motorCmd[FR_0].velocity = 0;
                SendLowROS.motorCmd[FR_0].positionStiffness = Kp[0];
                SendLowROS.motorCmd[FR_0].velocityStiffness = Kv[0];
                SendLowROS.motorCmd[FR_0].torque = 0.65f;

                SendLowROS.motorCmd[FR_1].position = motorCommand[1];
                SendLowROS.motorCmd[FR_1].velocity = 0;
                SendLowROS.motorCmd[FR_1].positionStiffness = Kp[1];
                SendLowROS.motorCmd[FR_1].velocityStiffness = Kv[1];
                SendLowROS.motorCmd[FR_1].torque = 0.0f;

                SendLowROS.motorCmd[FR_2].position = motorCommand[2];
                SendLowROS.motorCmd[FR_2].velocity = 0;
                SendLowROS.motorCmd[FR_2].positionStiffness = Kp[2];
                SendLowROS.motorCmd[FR_2].velocityStiffness = Kv[2];
                SendLowROS.motorCmd[FR_2].torque = 0.0f;

                //------------rear right
                SendLowROS.motorCmd[RR_0].position = motorCommand[3];
                SendLowROS.motorCmd[RR_0].velocity = 0;
                SendLowROS.motorCmd[RR_0].positionStiffness = Kp[0];
                SendLowROS.motorCmd[RR_0].velocityStiffness = Kv[0];
                SendLowROS.motorCmd[RR_0].torque = 0.65f;

                SendLowROS.motorCmd[RR_1].position = motorCommand[4];
                SendLowROS.motorCmd[RR_1].velocity = 0;
                SendLowROS.motorCmd[RR_1].positionStiffness = Kp[1];
                SendLowROS.motorCmd[RR_1].velocityStiffness = Kv[1];
                SendLowROS.motorCmd[RR_1].torque = 0.0f;

                SendLowROS.motorCmd[RR_2].position = motorCommand[5];
                SendLowROS.motorCmd[RR_2].velocity = 0;
                SendLowROS.motorCmd[RR_2].positionStiffness = Kp[2];
                SendLowROS.motorCmd[RR_2].velocityStiffness = Kv[2];
                SendLowROS.motorCmd[RR_2].torque = 0.0f;

                //-------------left front
                SendLowROS.motorCmd[FL_0].position = motorCommand[6];
                SendLowROS.motorCmd[FL_0].velocity = 0;
                SendLowROS.motorCmd[FL_0].positionStiffness = Kp[0];
                SendLowROS.motorCmd[FL_0].velocityStiffness = Kv[0];
                SendLowROS.motorCmd[FL_0].torque = 0.65f;


                SendLowROS.motorCmd[FL_1].position = motorCommand[7];
                SendLowROS.motorCmd[FL_1].velocity = 0;
                SendLowROS.motorCmd[FL_1].positionStiffness = Kp[1];
                SendLowROS.motorCmd[FL_1].velocityStiffness = Kv[1];
                SendLowROS.motorCmd[FL_1].torque = 0.0f;

                SendLowROS.motorCmd[FL_2].position = motorCommand[8];
                SendLowROS.motorCmd[FL_2].velocity = 0;
                SendLowROS.motorCmd[FL_2].positionStiffness = Kp[2];
                SendLowROS.motorCmd[FL_2].velocityStiffness = Kv[2];
                SendLowROS.motorCmd[FL_2].torque = 0.0f;

                //------------ left hind
                SendLowROS.motorCmd[RL_0].position = motorCommand[9];
                SendLowROS.motorCmd[RL_0].velocity = 0;
                SendLowROS.motorCmd[RL_0].positionStiffness = Kp[0];
                SendLowROS.motorCmd[RL_0].velocityStiffness = Kv[0];
                SendLowROS.motorCmd[RL_0].torque = 0.65f;

                SendLowROS.motorCmd[RL_1].position = motorCommand[10];
                SendLowROS.motorCmd[RL_1].velocity = 0;
                SendLowROS.motorCmd[RL_1].positionStiffness = Kp[1];
                SendLowROS.motorCmd[RL_1].velocityStiffness = Kv[1];
                SendLowROS.motorCmd[RL_1].torque = 0.0f;

                SendLowROS.motorCmd[RL_2].position = motorCommand[11];
                SendLowROS.motorCmd[RL_2].velocity = 0;
                SendLowROS.motorCmd[RL_2].positionStiffness = Kp[2];
                SendLowROS.motorCmd[RL_2].velocityStiffness = Kv[2];
                SendLowROS.motorCmd[RL_2].torque = 0.0f;
                //------------
            }
        }
        memcpy(&SendLowLCM, &SendLowROS, sizeof(LowCmd));
        roslcm.Send(SendLowLCM);
        ros::spinOnce();
        loop_rate.sleep();
    }
    //fclose(f_st);
    return 0;
}
