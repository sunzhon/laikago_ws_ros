#include "neutron_laikago_joint_limit.h"

Laikago_joint_limit::Laikago_joint_limit()
{
  this->offset_j0 = 0.0;
  this->offset_j1 = 0.5;  //0.5
  this->offset_j2 = -1.1; //-1.1

  this->FR_0_LIMIT_MAX = 0.85;
  this->FR_0_MAX = 0.4;
  this->FR_0_CEN = 0.0;
  this->FR_0_MIN = -0.5;
  this->FR_0_LIMIT_MIN = -0.95;

  this->FR_1_LIMIT_MAX = 1.4;
  this->FR_1_MAX = 1.0;
  this->FR_1_CEN = 0.5;
  this->FR_1_MIN = -0.21;
  this->FR_1_LIMIT_MIN = -0.5;

  this->FR_2_LIMIT_MAX = -0.7;
  this->FR_2_MAX = -0.89;
  this->FR_2_CEN = -1.1;
  this->FR_2_MIN = -1.98;
  this->FR_2_LIMIT_MIN = -2.6;

  this->FL_0_LIMIT_MAX = 0.85;
  this->FL_0_MAX = 0.5;
  this->FL_0_CEN = 0.06;
  this->FL_0_MIN = -0.3;
  this->FL_0_LIMIT_MIN = -0.85;

  this->FL_1_LIMIT_MAX = 1.5;
  this->FL_1_MAX = 1.0;
  this->FL_1_CEN = 0.5;
  this->FL_1_MIN = -0.21;
  this->FL_1_LIMIT_MIN = -0.5;

  this->FL_2_LIMIT_MAX = -0.7;
  this->FL_2_MAX = -0.89;
  this->FL_2_CEN = -1.1;
  this->FL_2_MIN = -1.98;
  this->FL_2_LIMIT_MIN = -2.6;

  this->RR_0_LIMIT_MAX = 0.8;
  this->RR_0_MAX = 0.4;
  this->RR_0_CEN = 0.0;
  this->RR_0_MIN = -0.5;
  this->RR_0_LIMIT_MIN = -1.0;

  this->RR_1_LIMIT_MAX = 1.5;
  this->RR_1_MAX = 1.0;
  this->RR_1_CEN = 0.5;
  this->RR_1_MIN = -0.3;
  this->RR_1_LIMIT_MIN = -0.5;

  this->RR_2_LIMIT_MAX = -0.7;
  this->RR_2_MAX = -0.89;
  this->RR_2_CEN = -1.1;
  this->RR_2_MIN = -1.98;
  this->RR_2_LIMIT_MIN = -2.6;

  this->RL_0_LIMIT_MAX = 0.9;
  this->RL_0_MAX = 0.5;
  this->RL_0_CEN = 0.0;
  this->RL_0_MIN = -0.3;
  this->RL_0_LIMIT_MIN = -0.8;

  this->RL_1_LIMIT_MAX = 1.5;
  this->RL_1_MAX = 1.0;
  this->RL_1_CEN = 0.5;
  this->RL_1_MIN = -0.3;
  this->RL_1_LIMIT_MIN = -0.4;

  this->RL_2_LIMIT_MAX = -0.7;
  this->RL_2_MAX = -0.89;
  this->RL_2_CEN = -1.1;
  this->RL_2_MIN = -1.98;
  this->RL_2_LIMIT_MIN = -2.6;
}

float Laikago_joint_limit::move_joint_limit(int jointNo, float jointVal)
{
    /*   Fornt Right  */
    if( jointNo == 1)
    {
      if((jointVal+this->offset_j0) >= this->FR_0_MAX){return this->FR_0_MAX;}
      else if((jointVal+this->offset_j0) <= this->FR_0_MIN){return this->FR_0_MIN;}
      else{return (jointVal+this->offset_j0);}
    }
    else if( jointNo == 2)
    {
      if((jointVal+this->offset_j1) >= this->FR_1_MAX){return this->FR_1_MAX;}
      else if((jointVal+this->offset_j1) <= this->FR_1_MIN){return this->FR_1_MIN;}
      else{return (jointVal+this->offset_j1);}
    }
    else if( jointNo == 3)
    {
      if((jointVal+this->offset_j2) >= this->FR_2_MAX){return this->FR_2_MAX;}
      else if((jointVal+this->offset_j2) <= this->FR_2_MIN){return this->FR_2_MIN;}
      else{return (jointVal+this->offset_j2);}
    }

    /*   Fornt Left  */
    else if( jointNo == 4)
    {
      if((jointVal+this->offset_j0) >= this->FL_0_MAX){return this->FL_0_MAX;}
      else if((jointVal+this->offset_j0) <= this->FL_0_MIN){return this->FL_0_MIN;}
      else{return (jointVal+this->offset_j0);}
    }
    else if( jointNo == 5)
    {
      if((jointVal+this->offset_j1) >= this->FL_1_MAX){return this->FL_1_MAX;}
      else if((jointVal+this->offset_j1) <= this->FL_1_MIN){return this->FL_1_MIN;}
      else{return (jointVal+this->offset_j1);}
    }
    else if( jointNo == 6)
    {
      if((jointVal+this->offset_j2) >= this->FL_2_MAX){return this->FL_2_MAX;}
      else if((jointVal+this->offset_j2) <= this->FL_2_MIN){return this->FL_2_MIN;}
      else{return (jointVal+this->offset_j2);}
    }

    /*   Rare Right  */
    else if( jointNo == 7)
    {
      if((jointVal+this->offset_j0) >= this->RR_0_MAX){return this->RR_0_MAX;}
      else if((jointVal+this->offset_j0) <= this->RR_0_MIN){return this->RR_0_MIN;}
      else{return (jointVal+this->offset_j0);}
    }
    else if( jointNo == 8)
    {
      if((jointVal+this->offset_j1) >= this->RR_1_MAX){return this->RR_1_MAX;}
      else if((jointVal+this->offset_j1) <= this->RR_1_MIN){return this->RR_1_MIN;}
      else{return (jointVal+this->offset_j1);}
    }
    else if( jointNo == 9)
    {
      if((jointVal+this->offset_j2) >= this->RR_2_MAX){return this->RR_2_MAX;}
      else if((jointVal+this->offset_j2) <= this->RR_2_MIN){return this->RR_2_MIN;}
      else{return (jointVal+this->offset_j2);}
    }

    /*   Rare Left  */
    else if( jointNo == 10)
    {
      if((jointVal+this->offset_j0) >= this->RL_0_MAX){return this->RL_0_MAX;}
      else if((jointVal+this->offset_j0) <= this->RL_0_MIN){return this->RL_0_MIN;}
      else{return (jointVal+this->offset_j0);}
    }
    else if( jointNo == 11)
    {
      if((jointVal+this->offset_j1) >= this->RL_1_MAX){return this->RL_1_MAX;}
      else if((jointVal+this->offset_j1) <= this->RL_1_MIN){return this->RL_1_MIN;}
      else{return (jointVal+this->offset_j1);}
    }
    else if( jointNo == 12)
    {
      if((jointVal+this->offset_j2) >= this->RL_2_MAX){return this->RL_2_MAX;}
      else if((jointVal+this->offset_j2) <= this->RL_2_MIN){return this->RL_2_MIN;}
      else{return (jointVal+this->offset_j2);}
    }
//return 0;
}
