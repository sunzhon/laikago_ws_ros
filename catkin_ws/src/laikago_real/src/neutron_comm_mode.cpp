/************************************************************************
Copyright (c) 2018-2019, Unitree Robotics.Co.Ltd. All rights reserved.
Use of this source code is governed by the MPL-2.0 license, see LICENSE.
************************************************************************/

#include <ros/ros.h>
#include <pthread.h>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <laikago_msgs/LowCmd.h>
#include <laikago_msgs/LowState.h>
#include "laikago_sdk/laikago_sdk.hpp"
#include <std_msgs/Float32MultiArray.h>
#include "neutron_laikago_joint_limit.cpp"

using namespace laikago;
using namespace std;

//********************************************************
//*                                                      *
//*                     variable                         *
//*                                                      *
//********************************************************

float Kv[3] = {0};
float Kp[3] = {0};
LowCmd SendLowLCM = {0};
LowState RecvLowLCM = {0};
laikago_msgs::LowCmd SendLowROS;
laikago_msgs::LowState RecvLowROS;


float motorSignal[12] = { 0, 0, 0,   // fr_joint0, fr_joint1, fr_joint2
                          0, 0, 0,   // fl_joint0, fl_joint1, fl_joint2
                          0, 0, 0,   // rr_joint0, rr_joint1, rr_joint2
                          0, 0, 0 }; // rl_joint0, rl_joint1, rl_joint2

constexpr int motorDir[12] = {  FR_0, FR_1, FR_2,
                                FL_0, FL_1, FL_2,
                                RR_0, RR_1, RR_2,
                                RL_0, RL_1, RL_2 };

float motionTime = 0;

Control control(LOWLEVEL);
LCM roslcm;
boost::mutex mutex;

//********************************************************
//*                                                      *
//*               global function                        *
//*                                                      *
//********************************************************

void* update_loop(void* data)
{
    while(ros::ok){
        boost::mutex::scoped_lock lock(mutex);
        roslcm.Recv();
        lock.unlock();
        usleep(2000);
    }
}

// motor callback function
void motorCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
  int i = 0;
  for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
  {
    motorSignal[i] = *it;
    std::cout << i << ": " << motorSignal[i]<< std::endl;
    i++;
  }
  return;
}

// time callback function
void motionTimeCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
  int i = 0;
  for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
  {
    motionTime = *it;
    std::cout << motionTime << std::endl;
    i++;
  }
  return;
}


//********************************************************
//*                                                      *
//*                 main program                         *
//*                                                      *
//********************************************************

int main(int argc, char *argv[])
{

    std::cout << "WARNING: Control level is set to LOW-level." << std::endl
              << "Make sure the robot is hung up." << std::endl
              << "Press Enter to continue..." << std::endl;
    std::cin.ignore();

    ros::init(argc, argv, "nuetron_comm_node");
    ros::NodeHandle n;
    ros::Rate loop_rate(500);
    roslcm.SubscribeState();

    pthread_t tid;
    pthread_create(&tid, NULL, update_loop, NULL);

    Laikago_joint_limit jointLimit;

    SendLowROS.levelFlag = LOWLEVEL;
    for(int i = 1; i<13; i++){
        SendLowROS.motorCmd[i].mode = 0x0A;   // motor switch to servo (PMSM) mode
    }

    //********************************************************
    //*                                                      *
    //*           define publisher and subscriber            *
    //*                                                      *
    //********************************************************

    // creat publisher and subscriber
    ros::Subscriber motorSub = n.subscribe("/motor_position",1,motorCB);
    ros::Subscriber motionTimeSub = n.subscribe("/motion_time_topic",1,motionTimeCB);


    while (ros::ok()){

        roslcm.Get(RecvLowLCM);
        memcpy(&RecvLowROS, &RecvLowLCM, sizeof(LowState));
        //printf("%f\n",  RecvLowROS.motorState[FR_1].position);
        //printf("%f \t\t %f \t\t %f\n", motiontime, moveJoint[0], moveJoint[1]);

        // Read force sensor
/*        for(int i = 0; i < 4; i++)
        {
            for( int j = 0; j < 3; j++)
            {
              printf("%.3f\t",  RecvLowROS.motorState[motorDir[3*i+j]].position);
                 //printf("%.1f ",RecvLowROS.footForce[i] );
            }
        }
        printf("\n");
*/
        // gravity compensation
        SendLowROS.motorCmd[FR_0].torque = -0.65f;
        SendLowROS.motorCmd[FL_0].torque = +0.65f;
        SendLowROS.motorCmd[RR_0].torque = -0.65f;
        SendLowROS.motorCmd[RL_0].torque = +0.65f;

        if( motionTime >= 100){
            if( motionTime == 100){
                Kp[0] = 0.132; Kp[1] = 0.132; Kp[2] = 0.132;
                Kv[0] = 0.02; Kv[1] = 0.02; Kv[2] = 0.02;
            }

            if( motionTime == 500){
                Kp[0] = 0.5; Kp[1] = 0.15; Kp[2] = 0.15;
                Kv[0] = 0.02; Kv[1] = 0.02; Kv[2] = 0.02;
            }

            if( motionTime == 900){
                Kp[0] = 0.8; Kp[1] = 0.18; Kp[2] = 0.18;
                Kv[0] = 0.02; Kv[1] = 0.02; Kv[2] = 0.02;
            }

            if( motionTime == 1300){
                Kp[0] = 1.2; Kp[1] = 0.22; Kp[2] = 0.22;
                Kv[0] = 0.02; Kv[1] = 0.02; Kv[2] = 0.02;
            }

            if( motionTime == 1700){
                Kp[0] = 2.00; Kp[1] = 0.38; Kp[2] = 1.38;  //Kp[0] = 1.84;
                Kv[0] = 0.02; Kv[1] = 0.02; Kv[2] = 0.02;
            }

            //********************************************************
            //*                                                      *
            //*                move all leg laikago                  *
            //*                                                      *
            //********************************************************
            if( motionTime > 2000 ){

              for(int i = 0; i < 1 ; i++)
              {
                   for(int j = 0; j < 3; j++)
                   {
                     //printf("%.3f\t", motorSignal[3*i+j] );
                     //torque = -(RecvLow.motorStatus[3*i+j].velocity-0)*1.0f

                     if(j == 0)
                     {

                       
                       SendLowROS.motorCmd[motorDir[3*i+j]].position = motorSignal[3*i+j];
                       SendLowROS.motorCmd[motorDir[3*i+j]].velocity = 0;
                       SendLowROS.motorCmd[motorDir[3*i+j]].positionStiffness = Kp[0];
                       SendLowROS.motorCmd[motorDir[3*i+j]].velocityStiffness = Kv[0];
                       SendLowROS.motorCmd[motorDir[3*i+j]].torque = 0;
                       //printf("%.3f\t", motorSignal[3*i+j]);
                       //printf("%.3f\t", motorSignal[3*i+j]);
                       std::cout << 3*i+j << ": " << motorSignal[3*i+j]<< std::endl;
                       //printf("***************************************");
                       //printf("\n");
                       //cout << jointLimit.move_joint_limit(motorDir[3*i+j], motorSignal[3*i+j]) << "\t";
                     }
                     else if(j == 1)
                     {
                       SendLowROS.motorCmd[motorDir[3*i+j]].position = motorSignal[3*i+j];
                       //SendLowROS.motorCmd[motorDir[3*i+j]].position = jointLimit.move_joint_limit(motorDir[3*i+j], motorSignal[3*i+j]);
                       SendLowROS.motorCmd[motorDir[3*i+j]].velocity = 0;
                       SendLowROS.motorCmd[motorDir[3*i+j]].positionStiffness = Kp[1];
                       SendLowROS.motorCmd[motorDir[3*i+j]].velocityStiffness = Kv[1];
                       SendLowROS.motorCmd[motorDir[3*i+j]].torque = 0.0f;
                       //printf("%.3f\t", motorSignal[3*i+j]);
                       //printf("%.3f\t", motorSignal[3*i+j]);
                       //printf("***************************************");
                       //printf("\n");
                       //cout << 0.5 + jointLimit.move_joint_limit(motorDir[3*i+j], motorSignal[3*i+j]) << "\t";
                     }
                     else if(j == 2)
                     {
                       SendLowROS.motorCmd[motorDir[3*i+j]].position = 1 + motorSignal[3*i+j];
                       //SendLowROS.motorCmd[motorDir[3*i+j]].position =  jointLimit.move_joint_limit(motorDir[3*i+j], motorSignal[3*i+j]);
                       SendLowROS.motorCmd[motorDir[3*i+j]].velocity = 0;
                       SendLowROS.motorCmd[motorDir[3*i+j]].positionStiffness = Kp[2];
                       SendLowROS.motorCmd[motorDir[3*i+j]].velocityStiffness = Kv[2];
                       SendLowROS.motorCmd[motorDir[3*i+j]].torque = 0.0f;
                       //printf("%.3f\t", motorSignal[3*i+j]);
                       //printf("%.3f\t", motorSignal[3*i+j]);
                       //printf("***************************************");
                       //printf("\n");
                       //cout << -1.1 + jointLimit.move_joint_limit(motorDir[3*i+j], motorSignal[3*i+j]) << "\t";
                     }
                   }
              }
              printf("\n");
            }
        }
        memcpy(&SendLowLCM, &SendLowROS, sizeof(LowCmd));
        roslcm.Send(SendLowLCM);
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
