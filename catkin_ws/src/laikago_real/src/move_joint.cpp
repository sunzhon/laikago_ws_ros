//********************************************************
//*                                                      *
//*                 move joint control                   *
//*                                                      *
//********************************************************
// author: Worasuchad Haomachai
// contact: worasuch@gmail.com
// update: 7/09/2019
// version: 1.0.0

//********************************************************
//*                                                      *
//*                 desscription                         *
//*                                                      *
//********************************************************
// generate sin wave for moving joint lag of laikago


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float32MultiArray.h"
#include <sstream>

//********************************************************
//*                                                      *
//*                 ros variable                         *
//*                                                      *
//********************************************************
std_msgs::Float32MultiArray motorMsgs;
std_msgs::Float32MultiArray moveTimeMsgs;

//********************************************************
//*                                                      *
//*                 global variable                      *
//*                                                      *
//********************************************************
unsigned long int Tpi = 0;
float timerSignal = 0;
float motorSignal[12] = { 0, 0, 0,   // fr_joint0, fr_joint1, fr_joint2
                          0, 0, 0,   // fl_joint0, fl_joint1, fl_joint2
                          0, 0, 0,   // rr_joint0, rr_joint1, rr_joint2
                          0, 0, 0 }; // rl_joint0, rl_joint1, rl_joint2



int main(int argc, char **argv)
{
  ros::init(argc, argv, "move_joint_node");
  ros::NodeHandle n;
  ros::Rate loop_rate(500);

  //********************************************************
  //*                                                      *
  //*           define publisher and subscriber            *
  //*                                                      *
  //********************************************************

  // creat publisher and subscriber
  ros::Publisher motorPub = n.advertise<std_msgs::Float32MultiArray>("/motor_topic",1);
  ros::Publisher motionTimePub = n.advertise<std_msgs::Float32MultiArray>("/motion_time_topic",1);

  while (ros::ok())
  {
    timerSignal++;

    motorMsgs.data.clear();
    moveTimeMsgs.data.clear();

    // move lag
    if(timerSignal > 2000)
    {
        Tpi++;
        //joint1 = 0.25 * sin(4*M_PI*Tpi/1000.0);
        //joint2 = 0.4 * sin(4*M_PI*Tpi/1000.0);

        for(int i = 0; i < 4; i++)
        {
          for(int j = 0; j < 3; j++)
          {
            if(j == 1)
            {
              motorMsgs.data.push_back(0.25 * sin(4*M_PI*Tpi/1000.0));
              //motorMsgs.data.push_back(0.1);
            }
            else if(j == 2)
            {
              motorMsgs.data.push_back(0.4 * sin(4*M_PI*Tpi/1000.0));
              //motorMsgs.data.push_back(0.1);
            }
            else
            {
              motorMsgs.data.push_back(0.0);
            }
          }
        }
    }

    moveTimeMsgs.data.push_back(timerSignal);

    //motorPub.publish(motorMsgs);
    motionTimePub.publish(moveTimeMsgs);

    // wait
    ros::spinOnce();
    loop_rate.sleep();
  }
  return 0;
}
