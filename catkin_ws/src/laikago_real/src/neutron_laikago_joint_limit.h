

#ifndef NEUTRON_LAIKAGO_JOINT_LIMIT_H
#define NEUTRON_LAIKAGO_JOINT_LIMIT_H

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <stdint.h>

using namespace std;

class Laikago_joint_limit
{
public:
  /* constructor */
  Laikago_joint_limit();

  /* constructor */
  float move_joint_limit(int jointNo, float jointVal);

private:
  float offset_j0;
  float offset_j1;
  float offset_j2;

  float FR_0_LIMIT_MAX;
  float	FR_0_MAX;
  float	FR_0_CEN;
  float	FR_0_MIN;
  float	FR_0_LIMIT_MIN;

  float	FR_1_LIMIT_MAX;
  float	FR_1_MAX;
  float	FR_1_CEN;
  float	FR_1_MIN;
  float	FR_1_LIMIT_MIN;

  float	FR_2_LIMIT_MAX;
  float	FR_2_MAX;
  float	FR_2_CEN;
  float	FR_2_MIN;
  float	FR_2_LIMIT_MIN;

  float	FL_0_LIMIT_MAX;
  float	FL_0_MAX;
  float	FL_0_CEN;
  float	FL_0_MIN;
  float	FL_0_LIMIT_MIN;

  float	FL_1_LIMIT_MAX;
  float	FL_1_MAX;
  float	FL_1_CEN;
  float	FL_1_MIN;
  float	FL_1_LIMIT_MIN;

  float	FL_2_LIMIT_MAX;
  float	FL_2_MAX;
  float	FL_2_CEN;
  float	FL_2_MIN;
  float	FL_2_LIMIT_MIN;

  float	RR_0_LIMIT_MAX;
  float	RR_0_MAX;
  float	RR_0_CEN;
  float	RR_0_MIN;
  float	RR_0_LIMIT_MIN;

  float	RR_1_LIMIT_MAX;
  float	RR_1_MAX;
  float	RR_1_CEN;
  float	RR_1_MIN;
  float	RR_1_LIMIT_MIN;

  float	RR_2_LIMIT_MAX;
  float	RR_2_MAX;
  float	RR_2_CEN;
  float	RR_2_MIN;
  float	RR_2_LIMIT_MIN;

  float	RL_0_LIMIT_MAX;
  float	RL_0_MAX;
  float	RL_0_CEN;
  float	RL_0_MIN;
  float	RL_0_LIMIT_MIN;

  float	RL_1_LIMIT_MAX;
  float	RL_1_MAX;
  float	RL_1_CEN;
  float	RL_1_MIN;
  float	RL_1_LIMIT_MIN;

  float	RL_2_LIMIT_MAX;
  float	RL_2_MAX;
  float	RL_2_CEN;
  float	RL_2_MIN;
  float	RL_2_LIMIT_MIN;
};

#endif
